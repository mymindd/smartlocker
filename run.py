# from flask import Flask, render_template
# import paho.mqtt.client as mqtt

# app = Flask(__name__)
# client = mqtt.Client('web')
# client.connect('172.30.222.51', port=1883)
# @app.route("/")
# def index():
#     msg = 'change'
#     client.publish('test/led-change','change')
#     return render_template('index.html',msg=msg)

import smartlocker

if __name__ == '__main__':
    app = smartlocker.create_app()
    app.run(host='127.0.0.1', port=8080, debug=True, threaded=True)
