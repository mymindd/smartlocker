from flask_allows import Allows 
from flask import request
from flask_login import current_user

allows = Allows(identity_loader=lambda: current_user)

def is_admin(ident, request):
    return 'admin' in ident.roles
