import mongoengine as me
import datetime

from . import User

class Locker(me.Document):
    name = me.StringField(required=True,default='-')
    status_opened = me.BooleanField(default=False)
    renter = me.ReferenceField(User, dbref=True)
    size = me.StringField(default='S')
    availability = me.BooleanField(default=True)
    started_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.now)
    expired_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.now)

    meta = {'collection': 'lockers'}
