import mongoengine as me
import datetime

from flask_login import UserMixin, LoginManager

class User(me.Document, UserMixin):
    username = me.StringField(required=True, unique=True)
    password = me.StringField()
    points = me.IntField(default='0', max_value=None, min_value=0)
    email = me.StringField()
    first_name = me.StringField(required=True)
    last_name = me.StringField(required=True)
    
    status = me.StringField(default='disactive')
    roles = me.StringField(default='user')
    phone_num = me.StringField(required=True, default='-')
    created_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.utcnow)
    update_date = me.DateTimeField(required=True,
                                    default=datetime.datetime.utcnow)

    meta = {'collection': 'users'}

# @login_manager.user_loader
# def load_user(user_id):
#     return User.objects(pk=user_id).first()
