import mongoengine as me
import datetime

from . import Locker

class Statuslocker(me.Document):
    locker = me.ReferenceField(Locker, dbref=True)
    opened = me.BooleanField(default=False)

    meta = {'collection': 'statuslockers'}
