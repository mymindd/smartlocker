from flask_mongoengine import MongoEngine
from .users import User
from .lockers import Locker

__all__ = [User, Locker]

db = MongoEngine()

def init_db(app):
    db.init_app(app)
