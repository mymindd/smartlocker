from flask import (Blueprint, 
                    request,
                    redirect,
                    url_for,
                    render_template)
from flask_login import (current_user,
                        login_required)
from .. import models
from .. import acl
from .. import mqtt
from smartlocker.forms import (AddLockersForm, 
                              ReserveLockerForm)
import datetime

module = Blueprint('dashboard', __name__, url_prefix='/dashboard')

@module.route('/', methods=['GET', 'POST'])
@login_required
def index():
    lockers=models.Locker.objects(renter=current_user._get_current_object())
    curr_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return render_template('dashboard/index.html',
                            user=current_user,
                            lockers=lockers,
                            curr_time=curr_time)


@module.route('/lockers')
@login_required
def lockers():
    lockers = models.Locker.objects() 
    return render_template('dashboard/lockers.html',
                            user=current_user,
                            lockers=lockers)

@module.route('/addlockers', methods=['GET', 'POST'])
@login_required
@acl.allows.requires(acl.is_admin)
def addlocker():
    form = AddLockersForm()
    if not form.validate_on_submit():
        return render_template('/dashboard/addlockers.html', 
                                form=form,
                                user=current_user)
    data = form.data.copy()
    data.pop('csrf_token')
    locker = models.Locker(**data).save()

    return redirect(url_for('dashboard.lockers'))

@module.route('/<locker_id>/reserve', methods=['GET', 'POST'])
@login_required
def reserve(locker_id):
    locker = models.Locker.objects.get(id=locker_id)
    # print('got', datetime.datetime.now())
    if locker.availability == False:
        return redirect(url_for('dashboard.lockers'))
    hours = [1, 3, 6, 9, 12]
    form = ReserveLockerForm()
    form.hour.choices = [(h, str(h)) for h in hours]
    if not form.validate_on_submit():
        return render_template('/dashboard/reservation.html',
                                user=current_user,
                                form=form)
    exp=datetime.datetime.now()
    if form.hour.data in [1,3,6,9,12]:
        # print('dalta')
        exp=datetime.timedelta(hours=form.hour.data)
        locker.update(started_date=datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                expired_date=(datetime.datetime.now()+exp).strftime("%Y-%m-%d %H:%M:%S"),
                 renter=current_user._get_current_object(),
                 availability=False)
        locker.save()
    return redirect(url_for('dashboard.lockers'))

@module.route('openlocker/<locker_id>', methods=['GET', 'POST'])
@login_required
def openlocker(locker_id):
    locker = models.Locker.objects.get(id=locker_id)
    if locker.availability == True:
        return redirect(url_for('dashboard.index'))
    else:
        mqtt.openlocker.openLocker(locker.name)
    return 'open'
