from flask import (Blueprint, 
                    request,
                    redirect,
                    url_for,
                    render_template,
                    jsonify)
from flask_login import (current_user,
                        login_required)
from .. import models
from .. import acl
from .. import mqtt
from smartlocker.forms import (AddLockersForm, 
                              ReserveLockerForm)
import datetime

module = Blueprint('lockers', __name__, url_prefix='/lockers')


@module.route('/<locker_id>/status', methods=['GET', 'POST'])
@login_required
def status(locker_id):
    locker=models.Locker.objects.get(id=locker_id,
                                      renter=current_user._get_current_object())
    curr_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    return jsonify(dict(id=str(locker.id),
                        opened_status=locker.status_opened,
                        ))


