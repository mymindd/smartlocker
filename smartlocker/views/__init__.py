from flask import Flask
from . import dashboard
from . import account
from . import lockers


def register_blueprint(app):
    app.register_blueprint(dashboard.module)
    app.register_blueprint(account.module)
    app.register_blueprint(lockers.module)
