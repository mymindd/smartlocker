from flask import (Blueprint, 
                   request, 
                   render_template, 
                   redirect, 
                   url_for)
from smartlocker.forms import LoginForm, RegisterForm 
from flask_login import (UserMixin, 
                        login_user, 
                        login_required, 
                        logout_user, 
                        current_user)
from .. import models
from werkzeug.security import generate_password_hash, check_password_hash
import datetime

module = Blueprint('main', __name__, url_prefix='/')

@module.route('/')
def index():
    form = LoginForm()
    return render_template('index.html',form=form)

@module.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated == True:
        return redirect(url_for('dashboard.index'))
    form = LoginForm()
    if not form.validate_on_submit():
        return render_template('/account/login.html', form=form)
   
    user = models.User.objects(username=form.username.data).first() # query from database
    if user:
        if check_password_hash(user.password, form.password.data):
            login_user(user)
            return redirect(url_for('dashboard.index'))

    return render_template('/account/login.html',form=form)

@module.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))

@module.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated == True:
        return redirect(url_for('dashboard.index'))
    form = RegisterForm()
    if not form.validate_on_submit():
        return render_template('/account/register.html', form=form)
    print('got on submit :',form.username.data)
    pass_hash = generate_password_hash(form.password.data, method='sha512')
    user = models.User(username=form.username.data,
                       email=form.email.data,
                       password=pass_hash,
                       first_name=form.first_name.data,
                       last_name=form.last_name.data,
                       phone_num=form.phone.data,
                       status='active',
                       points=0).save()
    return redirect(url_for('main.login'))
