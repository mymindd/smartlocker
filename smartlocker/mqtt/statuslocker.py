import paho.mqtt.client as mqtt
import threading


locker_report_status = None

def on_connect(mqttc, obj, flags, rc):
    print("rc: " + str(rc))


def on_message(mqttc, obj, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))


def on_publish(mqttc, obj, mid):
    print("mid: " + str(mid))


def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(mqttc, obj, level, string):
    print(string)

class LockerReportStatus(threading.Thread):
    def __init__(self):
        super().__init__()
        self.mqttc = mqtt.Client()
        self.mqttc.on_message = on_message
        self.mqttc.on_connect = on_connect
        self.mqttc.on_publish = on_publish
        self.mqttc.on_subscribe = on_subscribe

    def run(self):
    # Uncomment to enable debug messages
    # mqttc.on_log = on_log
        self.mqttc.connect("172.30.222.51", 1883, 60)
        self.mqttc.subscribe("locker/status", 0)

        self.mqttc.loop_forever()


def init_locker_report_status(app):
    locker_report_status = LockerReportStatus()
    locker_report_status.start()
