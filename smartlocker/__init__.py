__version__ = '0.0.1'

from flask import Flask
from flask_login import LoginManager

from . import views
from . import models
from . import default_settings
from . import mqtt

login_manager = LoginManager()

def init_login_manager(app):
    login_manager.init_app(app)
    login_manager.login_view = 'main.login'

@login_manager.user_loader
def load_user(user_id):
    return models.User.objects(pk=user_id).first()

def create_app():
    app = Flask(__name__)
    app.config.from_object(default_settings)
    app.config.from_envvar('SMARTLOCKER_SETTINGS', silent=True)
    
    models.init_db(app)
    init_login_manager(app)

    views.register_blueprint(app)
    
    return app
