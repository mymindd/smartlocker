from .users import (LoginForm, 
                    RegisterForm)

from .lockers import (AddLockersForm, 
                    ReserveLockerForm)
