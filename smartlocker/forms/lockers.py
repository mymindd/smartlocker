from flask_wtf import FlaskForm
from wtforms import Form
from wtforms import fields
from wtforms import validators

class AddLockersForm(FlaskForm):
    name = fields.TextField('Name',
                    validators=[validators.InputRequired(), 
                    validators.Length(min=2)])
    size = fields.TextField('Size', 
                    validators=[validators.InputRequired()])

class ReserveLockerForm(FlaskForm):
    hour = fields.SelectField('Hour', coerce=int)

