from flask_wtf import FlaskForm
from wtforms import Form
from wtforms import fields
from wtforms.fields import html5
from wtforms import validators

class LoginForm(FlaskForm):
    username = fields.TextField('Username',
            validators=[validators.InputRequired()])
    password = fields.PasswordField('Password',
            validators=[validators.InputRequired()])

class RegisterForm(FlaskForm):
    username = fields.TextField('Username',
                    validators=[validators.InputRequired(), 
                    validators.Length(min=3)])
    email = html5.EmailField('Email', 
                validators=[validators.InputRequired(), 
                validators.Email()])
    password = fields.PasswordField('Password', 
                                validators=[validators.InputRequired(), 
                                validators.Length(min=6), 
                                validators.EqualTo('password_conf', 
                                message="password mismatch")])
    password_conf = fields.PasswordField('Confirm Password', 
                                validators=[validators.InputRequired()])
    first_name = fields.TextField('First Name', 
                                   validators=[validators.InputRequired()])
    last_name = fields.TextField('Last Name', 
                                  validators=[validators.InputRequired()])
    phone = html5.TelField('Phone Number',
                            validators=[validators.InputRequired(), 
                            validators.Length(min=10, max=10)])
