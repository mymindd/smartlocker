function openLocker(id) {
	$.getJSON('openlocker/'+id);
}
function loadDoc(id) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
			var json = this.responseText;
			obj = JSON.parse(json);
			
			if (obj.opened_status == true){
      document.getElementById(id).style.color = "green";
    }
			else if(obj.opened_status == false){
			document.getElementById(id).style.color = "red";
			}
		}
  };
  xhttp.open("GET", "http://localhost:8080/lockers/"+id+"/status", true);
  xhttp.send();
}
